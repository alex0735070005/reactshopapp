export const addOrderAction = (dispatch, order) => 
{
    fetch('http://localhost:9000/add-order', {
        method:'POST',
        body:JSON.stringify(order),
        headers: {'Content-Type': 'application/json'},
    }).then((resp)=>{
        return resp.json();
    }).then(()=>{
        dispatch({type:'CLEAR_CART'})
    })
}