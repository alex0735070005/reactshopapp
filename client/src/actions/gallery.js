export const getItemsAction = (dispatch) => 
{
    let promise = new Promise((resolve, reject) => {
        // 1. Создаём новый объект XMLHttpRequest
        var xhr = new XMLHttpRequest();

        // 2. Конфигурируем его: GET-запрос на URL 'phones.json'
        xhr.open(
            'GET', 
            'https://api.unsplash.com/photos/?per_page=12&client_id=ca5a2a324ba06f2cf8bede88a989bb6c2f5f87730032b3c6256b72888f2cc94c', 
            false
        );

        // 3. Отсылаем запрос
        xhr.send();

        // 4. Если код ответа сервера не 200, то это ошибка
        if (xhr.status != 200) {
            // обработать ошибку
            reject(xhr)
        } else {          
            resolve(xhr.responseText);
        }
    })

    promise
    .then((textJson)=>{       
        return JSON.parse(textJson);
    })
    .then((data)=>{
        dispatch({type:"SET_ITEMS", data})
    })
    .catch((xhr)=>{        
        let er =  {
            status:xhr.status, 
            text: JSON.parse(xhr.responseText)
        }
        alert(er.text.errors[0]);
    })
}