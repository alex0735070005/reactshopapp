export const getProductsAction = (dispatch) => 
{
    fetch('http://localhost:9000/get-products', {
        method:'POST'
    }).then((resp)=>{
        return resp.json();
    }).then((d)=>{
        dispatch({type:'SET_PRODUCTS', data:d})
    })
}