import { combineReducers } from 'redux';
import cart from './cart';
import catalog from './catalog';
import gallery from './gallery';


const rootReducer = combineReducers({
    cart,
    catalog,
    gallery
})

export default rootReducer;