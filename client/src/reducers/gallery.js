
const initialState = {
  items:[]
}


const gallery = (state = initialState, action) => {
    
    switch (action.type) {

      case 'SET_ITEMS':
        return {
          ...state,
          items:[...action.data]
        }
  
      default:
        return state
    }
}

export default gallery;