const initialState = {
  totalQuantity:0,
  totalSum:0,
  phone:"",
  isValidPhone:false,
  products:[]
}

const addProduct = (productsCart, data) => {
  
  let up = false;

  // copy state productsCart
  let products = productsCart.map((item)=>{
      if(item.id === data.id){
          up = true;
          return {
              ...item, 
              quantity:item.quantity + 1,
              sum:item.sum + item.price
          }
      }
      return item;
  })

  if(!up){
      products.push({
          ...data,
          quantity:1,
          sum:data.price
        });
  }
  return products;
}

const removeProduct = (productsCart, data) => {
 
  let up = null;

  // copy state productsCart
  let products = productsCart.map((item, key)=>{
      if(item.id === data.id && data.quantity > 1){
          return {
              ...item, 
              quantity:item.quantity - 1,
              sum:item.sum - item.price
          }
      }else if(item.id === data.id){
          up = key;
      }
      return item;
  })
  
  if(up !== null){
      products.splice(up, 1);
  }
  return products;
}

const cart = (state = initialState, action) => {
  
    
    switch (action.type) {

      case 'ADD_PRODUCT':
        return {
          ...state,
          products:addProduct(state.products, action.data),
          totalQuantity: state.totalQuantity + 1,
          totalSum:state.totalSum + action.data.price
        }
      
      case 'REMOVE_PRODUCT':
      return {
        ...state,
        products:removeProduct(state.products, action.data),
        totalQuantity: state.totalQuantity - 1,
        totalSum:state.totalSum - action.data.price
      }

      case 'UPDATE_PHONE':
      return {
        ...state,
        phone: action.phone,
        isValidPhone:action.isValidPhone
      }

      case 'CLEAR_CART':
      return {
        ...initialState
      }

      default:
        return state
    }
}

export default cart;