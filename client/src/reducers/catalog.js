
const initialState = {
  products:[]
}


const catalog = (state = initialState, action) => {
    
    switch (action.type) {

      case 'SET_PRODUCTS':
        return {
          ...state,
          products:[...action.data]
        }
  
      default:
        return state
    }
}

export default catalog;