import React, {Component} from 'react';
import { connect } from 'react-redux';
import {getItemsAction} from '../../../actions/gallery';
import './style.scss';

class Gallery extends Component {

    componentDidMount(){
        this.props.getItems();
    }

    render(){
        return (
            <div className="gallery">
                {
                    this.props.items.map((item, index)=>
                        <img key={index} src={item.urls.small} />
                    )
                }
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
      items: state.gallery.items
    }
}
  
const mapDispatchToProps = (dispatch) => {
    return {
        getItems:()=>{
            getItemsAction(dispatch)
        }
    }
}


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Gallery)