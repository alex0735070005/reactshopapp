import React, {Component} from 'react';
import { connect } from 'react-redux';

class InputPhone extends Component {
    
    constructor(props) {
        super(props);
       
        this.state = {
            valid:true,
            value:""
        }
    }

    change = (e) =>
    {
        let value = e.target.value;

        if(/^\+380[0-9]{9}$/.test(value)){
            this.setState({valid:true, value});
            this.props.onChange(e, true);
        }else{
            this.setState({valid:false, value});
            this.props.onChange(e, false);
        }
    }

    render() {
        const {
            placeholder
        } = this.props;

        const {valid, value} = this.state;

        return (
            <div>
                <input
                    type="text"
                    className={valid ? "form-control": "form-control danger"}
                    placeholder={placeholder}
                    onInput={this.change}
                />
                {!valid && value && <span>+380000000000</span>}
            </div>
        )
    }
}

export default InputPhone