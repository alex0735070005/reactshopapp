import React from 'react';

const Header = function(props)
{
    return (
        <div onClick={props.showCart} id="cart-header">
            quantity:{props.totalQuantity} / sum: {props.totalSum}
        </div>
    )
}


export default Header;