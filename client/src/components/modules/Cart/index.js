import React, {Component} from 'react';
import { connect } from 'react-redux';

import Head from './Head';
import Body from './Body';
import Header from './Header';
import InputPhone from './InputPhone';
import {addOrderAction} from '../../../actions/cart';

import './style.scss';

class Cart extends Component
{
    constructor(props){
        super(props);

        this.state = {
            show:false,
            showErrorSend:false
        }
    }

    sendOrder = () =>{
        if(!this.props.isValidPhone){
            this.setState({showErrorSend:true});
            setTimeout(()=>{
                this.setState({showErrorSend:false});
            }, 5000)
        }else{
            this.props.addOrder(this.props);
        }
    }

    showCart = () => {
        this.setState({
            show:!this.state.show
        })
    }

    changePhone = (e, isValidPhone) =>{
        this.props.updatePhone(e.target.value, isValidPhone);
    }

    render(){

        const {
            totalQuantity,
            totalSum,
            products,
            isValidPhone
        } = this.props;

        return (
            <div id="cart">
                <Header 
                    totalQuantity   = {totalQuantity} 
                    totalSum        = {totalSum} 
                    showCart        = {this.showCart}
                />
                {  
                    this.state.show &&
                    (<div id="cart-body">
                        <table className="table table-bordered">
                            <Head/>
                            <Body products = {products} />
                        </table>
                        {
                        products.length ?
                        <div className="d-flex flex-wrap align-items-start justify-content-between">
                            <button onClick={this.sendOrder} className="btn btn-primary">
                                {!isValidPhone ? "Create order": "Send order"}
                            </button>
                            <div>
                                <InputPhone 
                                    placeholder="input your phone"
                                    onChange={this.changePhone}
                                />
                            </div>
                            {this.state.showErrorSend && <span className="w-100 mt-3">Input phone please</span>}
                        </div>
                        : ''
                        }
                    </div>)
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
      products:      state.cart.products,
      totalQuantity: state.cart.totalQuantity,
      totalSum:      state.cart.totalSum,
      isValidPhone:  state.cart.isValidPhone,
      phone:         state.cart.phone,
    }
}
  
const mapDispatchToProps = (dispatch) => {
    return {
        removeProduct:(d)=>{
            dispatch({type:'REMOVE_PRODUCT', data:d})
        },
        updatePhone:(phone, isValidPhone)=>{
            dispatch({type:'UPDATE_PHONE', phone, isValidPhone})
        },
        addOrder:(order)=>{
            addOrderAction(dispatch, order);
        }
    }
}


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Cart)