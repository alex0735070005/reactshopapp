var express = require('express');
var mysql = require('mysql');
var path = require('path');
var app = express();

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "1111",
  database: "shopapp"
});

app.use(express.json());

app.options("/*", function(req, res, next){
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  res.send(200);
})

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '/public/index.html'));
});


function insertProducts(data)
{ 
  let sql = `INSERT INTO 
  orders(name, sum, phone) 
  VALUES(
    "${data['name']}",
      ${data['sum']},
    "${data['phone']}")`;

    con.query(sql, function (err, result) {
      if (err) throw err;
      console.log("Result: " + result);
    });
}

app.post('/get-products', function (req, res) 
{
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.status(200);

  let sql = "SELECT * FROM products";

  con.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
});

app.post('/add-order', function (req, res) 
{
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.status(200);

  console.log(req);

  let products = req.body.products;

  products.forEach(product => {
    insertProducts({
      name:product.name,
      sum:product.sum,
      phone:req.body.phone
    });
  });

  res.send({answer:'success'});
});

var dir = path.join(__dirname, 'public');
app.use(express.static(dir));

app.listen(9000, function () {
  console.log('Example app listening on port 9000!');
});